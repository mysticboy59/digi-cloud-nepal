# Article Digitization Project
Article Digitization is a iOS project based on crowdsourcing model that will enable micro workers to work in chunk of small tasks (images) of newspaper article. The mobile workers would be converting those chuck images of article into digital text by typing them and sending them to server. After the task submitted is moderated and validated, the worker is paid a sum of amount.

This project is a demo project developed in iOS platform for implementing mobile microwork. This project uses a web service created by brother and co-worker.

# Team

This project is created by a team of two members. I am Rajan Maharjan (iOS app developer) and my brother Aman Maharjan (Web Service developer). We created this project in m2workhackathon event in two days.

# m2workhackathon.org

m2workhackathon is a global mobile microwork challenge. For more information on Hackathon please visit http://www.m2workhackathon.org.

# Conclusion

Clone or fork the project as you wish. For running this application you might need to download service project as well and run app in localhost. We will be hosting our service soon in server so that you can directly test the app.