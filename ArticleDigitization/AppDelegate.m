//
//  AppDelegate.m
//  ArticleDigitization
//
//  Created by Rajan Maharjan on 9/13/12.
//  Copyright (c) 2012 Mystic Vision. All rights reserved.
//

#import "AppDelegate.h"

#import "LoginViewController.h"
#import "SharedHelper.h"

/* Internet Reachability */
#import <sys/socket.h>
#import <netinet/in.h>
#import <SystemConfiguration/SystemConfiguration.h>

#import "TasksListViewController.h"
#import "SettingsViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    [self loadLoginViews];
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

/*
 Connectivity testing code pulled from Apple's Reachability Example: http://developer.apple.com/library/ios/#samplecode/Reachability
 */

+ (BOOL) hasConnectivity {
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (const struct sockaddr*)&zeroAddress);
    if(reachability != NULL) {
        //NetworkStatus retVal = NotReachable;
        SCNetworkReachabilityFlags flags;
        if (SCNetworkReachabilityGetFlags(reachability, &flags)) {
            if ((flags & kSCNetworkReachabilityFlagsReachable) == 0)
            {
                // if target host is not reachable
                return NO;
            }
            
            if ((flags & kSCNetworkReachabilityFlagsConnectionRequired) == 0)
            {
                // if target host is reachable and no connection is required
                //  then we'll assume (for now) that your on Wi-Fi
                return YES;
            }
            
            
            if ((((flags & kSCNetworkReachabilityFlagsConnectionOnDemand ) != 0) ||
                 (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0))
            {
                // ... and the connection is on-demand (or on-traffic) if the
                //     calling application is using the CFSocketStream or higher APIs
                
                if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0)
                {
                    // ... and no [user] intervention is needed
                    return YES;
                }
            }
            
            if ((flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN)
            {
                // ... but WWAN connections are OK if the calling application
                //     is using the CFNetwork (CFSocketStream?) APIs.
                return YES;
            }
        }
    }
    
    return NO;
}

#pragma - Custom Methods

-(void) loadLoginViews{
    
    LoginViewController *loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:loginViewController];
    
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.translucent = YES;
    
    [_window addSubview:self.navigationController.view];
}

- (void) showTabBarControllerAfterLogin:(UIView *) currentView {
    
    [currentView removeFromSuperview];
    
    self.tabBarController = [[UITabBarController alloc] init];
    [self.tabBarController.tabBar setSelectedImageTintColor:[UIColor blueColor]];
    
    TasksListViewController *taskViewController = [[TasksListViewController alloc] initWithNibName:@"TasksListViewController" bundle:nil];
    taskViewController.title = @"Tasks List";
    
    UITabBarItem *tasksViewTabBarItem = [[UITabBarItem alloc] initWithTitle:@"Task Lists" image:[UIImage imageNamed:@"task-list"] tag:1];
    taskViewController.tabBarItem = tasksViewTabBarItem;
    
    UINavigationController *tasksNavController = [[UINavigationController alloc] initWithRootViewController:taskViewController];
    tasksNavController.navigationBar.tintColor = [UIColor blackColor];
    tasksNavController.navigationBar.translucent = YES;

    SettingsViewController *settingsViewController = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
    settingsViewController.title = @"Account";
    UITabBarItem *settingsTabBarItem = [[UITabBarItem alloc] initWithTitle:@"Settings" image:[UIImage imageNamed:@"account"] tag:0];
    settingsViewController.tabBarItem = settingsTabBarItem;
    
    UINavigationController *settingsNavController = [[UINavigationController alloc] initWithRootViewController:settingsViewController];
    settingsNavController.navigationBar.tintColor = [UIColor blackColor];
    settingsNavController.navigationBar.translucent = YES;
    
    [self.tabBarController setViewControllers:[NSArray arrayWithObjects:tasksNavController, settingsNavController, nil] animated:YES];
    
    [_window addSubview:self.tabBarController.view];
}

- (void) logOutFromSystem {
    
    [_tabBarController.view removeFromSuperview];
    
    self.tabBarController = nil;
        
    [self loadLoginViews];
    [[SharedHelper helper] showMessage:@"Logout successful!" withColor:[UIColor greenColor]];
    
}
@end
