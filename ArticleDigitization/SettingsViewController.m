//
//  SettingsViewController.m
//  ArticleDigitization
//
//  Created by Rajan Maharjan on 9/14/12.
//  Copyright (c) 2012 Mystic Vision. All rights reserved.
//

#import "SettingsViewController.h"
#import "SharedHelper.h"
#import "ChangePasswordViewController.h"
#import "TransactionHistoryViewController.h"
#import "NSString+HTMLRemover.h"

@interface SettingsViewController ()

@property (nonatomic, strong) NSURLConnection *balanceConnection;
@property (nonatomic, strong) NSMutableData *balanceResponseData;

@property (nonatomic, strong) NSURLConnection *transactionConnection;
@property (nonatomic, strong) NSMutableData *transactionResponseData;

@property (nonatomic, strong) TransactionHistoryViewController *thVc;

@end

@implementation SettingsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        // You haven't anything used here
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    sectionHeaders = [NSArray arrayWithObjects:@"Profile", @"Earnings", nil];
    profileRowsLabels = [NSArray arrayWithObjects:@"Username", @"Password", @"Change Password", nil];
    earningRowsLabels = [NSArray arrayWithObjects:@"Total Balance", @"Pending Balance", @"Transaction History", nil];    

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    userProfileObjectsArray = [NSArray arrayWithObjects:
                                   [userDefaults objectForKey:@"UserMobileNumber"],
                                    @"********",
                               nil];
    
    userEarningObjectsArray = [[NSMutableArray alloc] initWithObjects:@"", @"", nil];
    
    logoutResponseData = [[NSMutableData alloc] init];
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    UIBarButtonItem *logoutBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Log Out" style:UIBarButtonItemStyleDone target:self action:@selector(logOut:)];
    logoutBarButtonItem.tintColor = [UIColor blueColor];
    self.navigationItem.rightBarButtonItem = logoutBarButtonItem;
    
    self.view.backgroundColor = [UIColor scrollViewTexturedBackgroundColor];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self retriveDataFromServer:@"get_balance"];    
}

#pragma - Cutom Methods

- (void) retriveDataFromServer:(NSString *) apiName {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *mobileNumber = [userDefaults objectForKey:@"UserMobileNumber"];
    NSString *token = [userDefaults objectForKey:@"UserToken"];
    
    NSString *encodedMobileNumber = [mobileNumber urlEncodeUsingEncoding:kCFStringEncodingUTF8];
    NSString *encodedToken = [token urlEncodeUsingEncoding:kCFStringEncodingUTF8];
    
    NSDictionary *JSONDictionary = [NSDictionary dictionaryWithObjects:
                                    [NSArray arrayWithObjects: encodedMobileNumber, encodedToken , nil]
                                                               forKeys:[NSArray arrayWithObjects: @"mobile_number", @"token", nil]];
    NSError *error = nil;
    
    // Converted JSONDictionary Object in JSON data Object
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:JSONDictionary
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *content = [NSString stringWithFormat:@"%@", jsonString];
    
    NSString *connectionString = [NSString stringWithFormat:@"%@/%@", SERVER_STRING,  apiName];
    
    NSLog(@"connection string : %@", connectionString);
    NSURL* url = [NSURL URLWithString:connectionString];
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:url];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[content dataUsingEncoding:NSUTF8StringEncoding]];
    
    if ( [apiName isEqualToString:@"get_transaction_history"] ) {
        
        self.transactionConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
        
        if (self.transactionConnection) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            
            [self.overlayView showInView:self.view withActivityIndicator:YES];
        }        
    }
    
    else {
        
        self.balanceConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];        
        if (self.balanceConnection) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            
            [self.overlayView showInView:self.view withActivityIndicator:YES];
        }
    }
}

#pragma - Self Methods

- (TransactionHistoryViewController *) thVc {
    
    if ( _thVc != nil )
        return _thVc;
    
    _thVc = [[TransactionHistoryViewController alloc] init];
    return _thVc;    
}

- (NSMutableData *) balanceResponseData {
    if ( _balanceResponseData != nil )
        return _balanceResponseData;
    
    _balanceResponseData = [[NSMutableData alloc] init];
    return _balanceResponseData;
}

- (NSMutableData *) transactionResponseData {
    if ( _transactionResponseData != nil )
        return _transactionResponseData;
    
    _transactionResponseData = [[NSMutableData alloc] init];
    return _transactionResponseData;
}

- (OverlayView *) overlayView {
    if ( _overlayView != nil )
        return _overlayView;
    
    _overlayView = [[OverlayView alloc] initWithFrame:CGRectMake(0.0, 0.0, IPHONE_WIDTH, IPHONE_HEIGHT)];
    return _overlayView;
}

- (void) viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Log Out

- (void) logOut: (id) sender {
    
    NSDictionary *JSONDictionary = [NSDictionary dictionaryWithObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"UserMobileNumber"]
                                                               forKey:@"mobile_number"];
    NSError *error = nil;
    
    // Converted JSONDictionary Object in JSON data Object
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:JSONDictionary
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *content = [NSString stringWithFormat:@"%@", jsonString];    

	NSString *connectionString = [NSString stringWithFormat:@"%@/logout", SERVER_STRING];
    
	NSURL* url = [NSURL URLWithString:connectionString];
	NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:url];
    
	[urlRequest setHTTPMethod:@"DELETE"];
	[urlRequest setHTTPBody:[content dataUsingEncoding:NSUTF8StringEncoding]];    
    
    logoutConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
    
    if ( logoutConnection ) {
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        [self.overlayView showInView:[APP_DELEGATE window] withActivityIndicator:YES];
	}
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [sectionHeaders count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50.0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *tableHeaderView = [[UIView alloc] init];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 50 / 2 - 10 , tableView.bounds.size.width - 10, 18)];
    
    label.text = [sectionHeaders objectAtIndex:section];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont boldSystemFontOfSize:18.0f];
    label.backgroundColor = [UIColor clearColor];
    
    [tableHeaderView addSubview:label];
    
    return tableHeaderView;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if ( section == 0 )
        return [profileRowsLabels count];
    
    return [earningRowsLabels count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if ( cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    
    // Configure the cell...
    
    if ( indexPath.section == 0) {
        cell.textLabel.text = [profileRowsLabels objectAtIndex:indexPath.row];
        
        if ( indexPath.row == [profileRowsLabels count] - 1 )
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        else
            cell.detailTextLabel.text = [userProfileObjectsArray objectAtIndex:indexPath.row];
    }
    
    else {
        cell.textLabel.text = [earningRowsLabels objectAtIndex:indexPath.row];
    
        if ( indexPath.row == [earningRowsLabels count] - 1 )
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        else
            cell.detailTextLabel.text = [userEarningObjectsArray objectAtIndex:indexPath.row];
    }

    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.

    if (  indexPath.section == 0 ) {
    
        ChangePasswordViewController *detailViewController = [[ChangePasswordViewController alloc] initWithNibName:@"ChangePasswordViewController" bundle:nil];
        detailViewController.title = @"Change Password";
        detailViewController.hidesBottomBarWhenPushed = YES;
        
        // Pass the selected object to the new view controller.
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    
    else {

        self.thVc.title = @"Transaction History";
        self.thVc.hidesBottomBarWhenPushed = YES;
        [self retriveDataFromServer:@"get_transaction_history"];        
        [self.navigationController pushViewController:self.thVc animated:YES];
    }
        
}

- (NSIndexPath *) tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ( ( indexPath.section == 0 && indexPath.row == [profileRowsLabels count] - 1 )
        || ( indexPath.section == 1 && indexPath.row == [earningRowsLabels count] - 1 ) )
        return indexPath;
    
    return nil;
}

#pragma - NSUrlConnection Delegates

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {

    if ( connection == logoutConnection )
        [logoutResponseData setLength:0];
    
    if ( connection == self.balanceConnection )
        [self.balanceResponseData setLength:0];
    
    if ( connection == self.transactionConnection )
        [self.transactionResponseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {

    if ( connection == logoutConnection )
        [logoutResponseData appendData:data];
    
    if ( connection == self.balanceConnection )
        [self.balanceResponseData appendData:data];
    
    if ( connection == self.transactionConnection )
        [self.transactionResponseData appendData:data];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
    if ( connection == logoutConnection ) {
        logoutConnection = nil;
        NSLog(@"LogOut Failed : %@", [error description]);
        [self.overlayView hide];
    }

    if ( connection == self.balanceConnection ) {
        self.balanceConnection = nil;
        NSLog(@"Balance Enquiry Failed : %@", [error description]);
        [self.overlayView hide];
    }
    
    if ( connection == self.transactionConnection ) {
        self.transactionConnection = nil;
        NSLog(@"Transaction List Enquiry Failed : %@", [error description]);
        [self.overlayView hide];
    }
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    NSError *error = nil;
    NSDictionary *jsonResponse = nil;
    
    if( connection == logoutConnection ) {
        
        jsonResponse = [NSJSONSerialization JSONObjectWithData:logoutResponseData options:NSJSONReadingAllowFragments error:&error];
    }
    
    else if ( connection == self.balanceConnection ) {
        jsonResponse = [NSJSONSerialization JSONObjectWithData:self.balanceResponseData options:NSJSONReadingAllowFragments error:&error];
    }
    
    else if ( connection == self.transactionConnection ) {
        jsonResponse = [NSJSONSerialization JSONObjectWithData:self.transactionResponseData options:NSJSONReadingAllowFragments error:&error];
    }
    
    if ( !jsonResponse ) {
        NSLog(@"Error is: %@", [error description]);
    }
    
    NSLog(@"Json : %@", jsonResponse);
    BOOL requestSuccess = [[jsonResponse objectForKey:@"success"] boolValue] ? YES : NO;
    
    
    if ( connection == logoutConnection ) {
        // Check here validity of Login using JSON response        
        
        if ( requestSuccess ) {
            
            // On Success Delete User Token
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults removeObjectForKey:@"UserToken"];
            [userDefaults removeObjectForKey:@"UserPassword"];
            [userDefaults removeObjectForKey:@"AutoFillPassword"];
            [userDefaults synchronize];
            
            [APP_DELEGATE logOutFromSystem];
        }
        
        else {
            [[SharedHelper helper] showMessage:[NSString stringWithFormat:@"Connection Error Occurred while logging out! %@", [error description] ] withColor:nil];
        }
        [self.overlayView hide];
    }
    
    if ( connection == self.balanceConnection ) {
        
        if ( requestSuccess ) {
            
            [userEarningObjectsArray replaceObjectAtIndex:0 withObject: [NSString stringWithFormat:@"%d",
             [[jsonResponse objectForKey:@"available"] integerValue]]];
            [userEarningObjectsArray replaceObjectAtIndex:1 withObject: [NSString stringWithFormat:@"%d",[[jsonResponse objectForKey:@"pending"] integerValue]]];
            
            [self.tableView reloadData];
        }
        else {
            [[SharedHelper helper] showMessage:[NSString stringWithFormat:@"Connection Error Occurred while Retriving Balance out! %@", [error description] ] withColor:nil];
        }
        [self.overlayView hide];
    }
    
    if ( connection == self.transactionConnection ) {
        
        if ( requestSuccess ) {
            NSArray * transactionHistoryArray = [[NSMutableArray alloc] initWithArray:[jsonResponse objectForKey:@"value"]];
            
            self.thVc.transactionHistoryArray = transactionHistoryArray;            
        }
        else {
            [[SharedHelper helper] showMessage:[NSString stringWithFormat:@"Connection Error Occurred while retreiving transaction! %@", [error description] ] withColor:nil];
        }
        [self.overlayView hide];        
    }
}

@end
