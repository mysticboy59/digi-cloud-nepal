//
//  LoginCell.m
//  ArticleDigitization
//
//  Created by Rajan Maharjan on 9/14/12.
//  Copyright (c) 2012 Mystic Vision. All rights reserved.
//

#import "LoginCell.h"

@implementation LoginCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma - UITextField Delegate

#pragma mark - TextFieldDelegates

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    
    if (self.indexPath.row == 0 )
        textField.returnKeyType = UIReturnKeyNext;
    else
        textField.returnKeyType = UIReturnKeyGo;
}

- (void) textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if ( textField.returnKeyType == UIReturnKeyGo ) {
        
        NSLog(@"Login Automatically");
    }
    
}

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    
//    BOOL value = [_delegate textFieldShouldBeginEditing:textField andIndexPath:self.indexPath];
    return YES;
}

- (BOOL) textFieldShouldEndEditing:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
//    [_delegate moveViewDownAfterHidingKeyboardWithIndexPath:self.indexPath andTextField:textField];
    
    return YES;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
//    BOOL value = [_delegate textFieldShouldReturn:textField andIndexPath:self.indexPath];
    [textField resignFirstResponder];
    return YES;
}

- (BOOL) textFieldShouldClear:(UITextField *)textField {
    return YES;
}
@end
