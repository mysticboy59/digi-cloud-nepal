//
//  TaskFormViewController.m
//  ArticleDigitization
//
//  Created by Rajan Maharjan on 9/14/12.
//  Copyright (c) 2012 Mystic Vision. All rights reserved.
//

#import "TaskFormViewController.h"
#import "OverlayView.h"
#import "SharedHelper.h"
#import "NSString+HTMLRemover.h"

@interface TaskFormViewController ()

@property (nonatomic, strong) IBOutlet UITextView *answerTextView;
@property (nonatomic, strong) IBOutlet UILabel *articleTitleLabel;
@property (nonatomic, strong) IBOutlet UIImageView *taskImageView;

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) OverlayView *overlayView;

@property (nonatomic, strong) NSMutableData *acceptReponseData;
@property (nonatomic, strong) NSURLConnection *acceptTaskConnection;

@property (nonatomic, strong) NSMutableData *submitReponseData;
@property (nonatomic, strong) NSURLConnection *submitsTaskConnection;           

@end

@implementation TaskFormViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    self.title = [NSString stringWithFormat:@"Task: %d-%d", self.currentTask.articleID, self.currentTask.chunkNO];
//    UIBarButtonItem *skipTasks = [[UIBarButtonItem alloc] initWithTitle:@"Skip" style:UIBarButtonItemStyleBordered target:self action:@selector(skipTasks:)];
//    self.navigationItem.rightBarButtonItem = skipTasks;

    self.articleTitleLabel.text = self.currentTask.title;
    [self retriveImageFormServer];
    
    originalImageRect = self.taskImageView.frame;
    originalImageRect.origin.y = originalImageRect.origin.y + 68.0;
    originalImageRect.size.height = originalImageRect.size.height - 68.0;
    
    [self showActionSheet];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma - self methods

- (OverlayView *) overlayView {
    if ( _overlayView != nil )
        return _overlayView;
    
    _overlayView = [[OverlayView alloc] initWithFrame:self.view.frame];
    return _overlayView;
}

- (NSMutableData *) acceptReponseData {
    if ( _acceptReponseData != nil )
        return _acceptReponseData;
    
    _acceptReponseData = [[NSMutableData alloc] init];
    return _acceptReponseData;
}

- (NSMutableData *) submitReponseData {
    if ( _submitReponseData != nil )
        return _submitReponseData;
    
    _submitReponseData = [[NSMutableData alloc] init];    
    return _submitReponseData;
}

#pragma - IBAction Methods

- (void) popCurrentViewController {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction) submitTask :(id)sender {
    
    /* Submit : Send remote request to Submit the Task
     * params : mobile_number, token, article_id, chunk_no, chunk_text
     */
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *mobileNumber = [userDefaults objectForKey:@"UserMobileNumber"];
    NSString *token = [userDefaults objectForKey:@"UserToken"];
    
    NSString *encodedMobileNumber = [mobileNumber urlEncodeUsingEncoding:kCFStringEncodingUTF8];
    NSString *encodedToken = [token urlEncodeUsingEncoding:kCFStringEncodingUTF8];
    NSString *encodeArticleID = [NSString stringWithFormat:@"%d", self.currentTask.articleID];
    NSString *encodedChunkNo = [NSString stringWithFormat:@"%d", self.currentTask.chunkNO];
    
    NSString *answerText = self.answerTextView.text;
    
    NSDictionary *JSONDictionary = [NSDictionary dictionaryWithObjects:
                                    [NSArray arrayWithObjects: encodedMobileNumber, encodedToken , encodeArticleID, encodedChunkNo, answerText, nil]
                                                               forKeys:[NSArray arrayWithObjects: @"mobile_number", @"token", @"article_id", @"chunk_no", @"chunk_text", nil]];
    NSError *error = nil;
    
    // Converted JSONDictionary Object in JSON data Object
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:JSONDictionary
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *content = [NSString stringWithFormat:@"%@", jsonString];
    
    NSString *connectionString = [NSString stringWithFormat:@"%@/submit_completed_task", SERVER_STRING];
    
    NSURL* url = [NSURL URLWithString:connectionString];
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:url];
    
    [urlRequest setHTTPMethod:@"PUT"];
    [urlRequest setHTTPBody:[content dataUsingEncoding:NSUTF8StringEncoding]];
    
    self.submitsTaskConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
    
    if (self.submitsTaskConnection) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        [self.overlayView showInView:self.view withActivityIndicator:YES];
    }
}

#pragma - Custom Methods

- (void) retriveImageFormServer {
    
    if ( !self.currentTask ) return;
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", SERVER_STRING, self.currentTask.chunkImagePath];
    [self.overlayView showInView:self.view withActivityIndicator:YES];
    [NSThread detachNewThreadSelector:@selector(loadImageInBackground:) toTarget:self withObject:urlString];    
}

- (void) loadImageInBackground:(NSString *) urlString {
        
    // Retrieve the remote image
    NSURL *imgURL = [NSURL URLWithString:urlString];
    NSData *imgData = [NSData dataWithContentsOfURL:imgURL];
    UIImage *img    = [[UIImage alloc] initWithData:imgData];

    // Image retrieved, call main thread method to update image, passing it the downloaded UIImage
    [self performSelectorOnMainThread:@selector(assignImageToImageView:) withObject:img waitUntilDone:YES];
}

- (void) assignImageToImageView:(UIImage *)img
{
    self.taskImageView.image = img;
    [self.overlayView hide];
}

- (void) skipTasks:(id) sender {
    NSLog(@"Skip Current Task and show next task!");
}

- (void) showActionSheet {
    
    UIActionSheet *acceptDecline = [[UIActionSheet alloc] initWithTitle:@"Want To work on this Task?" delegate:self cancelButtonTitle:@"Accept" destructiveButtonTitle:@"Decline" otherButtonTitles:nil];
    
    [acceptDecline showInView:self.view];    
}

#pragma - UIActionSheet Delegate

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if ( buttonIndex == 0 ) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    if ( buttonIndex == 1 ) {
        /* Accepted : Send remote request to lock the task
         * params : mobile_number, token, article_id, chunk_no
         */
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        NSString *mobileNumber = [userDefaults objectForKey:@"UserMobileNumber"];
        NSString *token = [userDefaults objectForKey:@"UserToken"];
        
        NSString *encodedMobileNumber = [mobileNumber urlEncodeUsingEncoding:kCFStringEncodingUTF8];
        NSString *encodedToken = [token urlEncodeUsingEncoding:kCFStringEncodingUTF8];
        NSString *encodeArticleID = [NSString stringWithFormat:@"%d", self.currentTask.articleID];
        NSString *encodedChunkNo = [NSString stringWithFormat:@"%d", self.currentTask.chunkNO];
        
        NSDictionary *JSONDictionary = [NSDictionary dictionaryWithObjects:
                                        [NSArray arrayWithObjects: encodedMobileNumber, encodedToken , encodeArticleID, encodedChunkNo, nil] forKeys:[NSArray arrayWithObjects: @"mobile_number", @"token", @"article_id", @"chunk_no", nil]];
        NSError *error = nil;
        
        // Converted JSONDictionary Object in JSON data Object
        NSData* jsonData = [NSJSONSerialization dataWithJSONObject:JSONDictionary
                                                           options:NSJSONWritingPrettyPrinted error:&error];
        
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSString *content = [NSString stringWithFormat:@"%@", jsonString];
        
        NSString *connectionString = [NSString stringWithFormat:@"%@/accept_task", SERVER_STRING];
        
        NSURL* url = [NSURL URLWithString:connectionString];
        NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:url];
        
        [urlRequest setHTTPMethod:@"PUT"];
        [urlRequest setHTTPBody:[content dataUsingEncoding:NSUTF8StringEncoding]];
        
        self.acceptTaskConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
        
        if (self.acceptTaskConnection) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            
            [self.overlayView showInView:self.view withActivityIndicator:YES];
        }
    }
}

- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
}

#pragma - Keyboard Notifications

- (void) keyboardWillShow:(NSNotification *)notification {
    
    CGFloat keyboardHeight;
    CGFloat animationDuration = 0.3;
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if(notification) {
        NSDictionary* keyboardInfo = [notification userInfo];
        CGRect keyboardFrame = [[keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
        animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
        
        if(UIInterfaceOrientationIsPortrait(orientation))
            keyboardHeight = keyboardFrame.size.height;
        else
            keyboardHeight = keyboardFrame.size.width;
    }
    
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    if(UIInterfaceOrientationIsPortrait(orientation))
        screenHeight = [UIScreen mainScreen].bounds.size.height;
    else
        screenHeight = [UIScreen mainScreen].bounds.size.width;
    
    __block CGRect frame = self.view.frame;
    
    originalViewFrameBeforeKeyboardAppear = frame;
    self.scrollView = [[UIScrollView alloc] initWithFrame:originalImageRect];
    
    NSLog(@"%f, %f, %f, %f", originalImageRect.origin.x, originalImageRect.origin.y + 50.0, originalImageRect.size.width, originalImageRect.size.height);
    
    UIImageView *taskImage = [[UIImageView alloc] initWithImage:self.taskImageView.image];
    self.taskImageView.hidden = YES;

    self.scrollView.tag = 100;
    
    [self.scrollView setBackgroundColor:[UIColor clearColor]];
    [self.scrollView setCanCancelContentTouches:NO];
    self.scrollView.clipsToBounds = YES;	// default is NO, we want to restrict drawing within our scrollview
    self.scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    
    [self.scrollView addSubview:taskImage];
    [self.scrollView setContentSize:CGSizeMake(self.taskImageView.frame.size.width, self.taskImageView.frame.size.height)];
    [self.scrollView setScrollEnabled:YES];
    self.scrollView.alpha = 0.0;    
    [self.view addSubview:self.scrollView];

    
    if (frame.origin.y + frame.size.height > screenHeight - keyboardHeight) {
        
        frame.origin.y = screenHeight - keyboardHeight - frame.size.height - 10;
        
        if (frame.origin.y < 0)
            frame.origin.y = 0;
        
        frame.origin.y = frame.origin.y - 130.0;
        
        
        [UIView animateWithDuration:animationDuration
                              delay:0.0
                            options:UIViewAnimationCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             self.view.frame = frame;
                         }
                         completion:nil];
        [UIView animateWithDuration:1.0 animations:^ {
            self.scrollView.alpha = 1.0;
        }];
        
    }
}

- (void) keyboardWillHide:(NSNotification *) notification {
    
    CGFloat keyboardHeight;
    CGFloat animationDuration = 0.3;
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if(notification) {
        NSDictionary* keyboardInfo = [notification userInfo];
        CGRect keyboardFrame = [[keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
        animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
        
        if(UIInterfaceOrientationIsPortrait(orientation))
            keyboardHeight = keyboardFrame.size.height;
        else
            keyboardHeight = keyboardFrame.size.width;
    }
    
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    if(UIInterfaceOrientationIsPortrait(orientation))
        screenHeight = [UIScreen mainScreen].bounds.size.height;
    else
        screenHeight = [UIScreen mainScreen].bounds.size.width;
    
    
    __block CGRect frame = self.view.frame;
    
    if (frame.origin.y < screenHeight - keyboardHeight) {
        
        frame.origin.y = originalViewFrameBeforeKeyboardAppear.origin.y;
        
        [UIView animateWithDuration:animationDuration
                              delay:0.0
                            options:UIViewAnimationCurveEaseIn | UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             self.view.frame = frame;
                         }
                         completion:nil];
    }
    
    if ( [self.view viewWithTag:100] ) {
        [[self.view viewWithTag:100] removeFromSuperview];
    }
    
    self.taskImageView.hidden = NO;
}

#pragma  - UITextView Delegate

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView {
    return YES;
}

- (BOOL) textViewShouldEndEditing:(UITextView *)textView {
    [textView resignFirstResponder];
    return YES;
}

- (BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@"\n"])
        [textView resignFirstResponder];
    return YES;
}

#pragma - NSUrlConnection Delegates

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    if (connection == self.acceptTaskConnection) {
        [self.acceptReponseData setLength:0];
    }
    
    if ( connection == self.submitsTaskConnection ) {
        [self.submitReponseData setLength:0];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if (connection == self.acceptTaskConnection) {
        [self.acceptReponseData appendData:data];
    }
    
    if ( connection == self.submitsTaskConnection ) {
        [self.submitReponseData appendData:data];
    }    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if (connection == self.acceptTaskConnection) {
        
        NSLog(@"Task Accpetance Failed : %@", [error description]);
        
        [self.overlayView hide];
        self.acceptTaskConnection = nil;
    }

    if (connection == self.submitsTaskConnection) {
        
        NSLog(@"Task Submission Failed : %@", [error description]);
        
        [self.overlayView hide];
        self.submitsTaskConnection = nil;
    }

}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if (connection == self.acceptTaskConnection) {
        
        // Check here validity of Login using JSON response
        
        NSError *error = nil;
        NSDictionary *jsonValue = [NSJSONSerialization JSONObjectWithData:self.acceptReponseData options:NSJSONReadingAllowFragments error:&error];
        
        if ( !jsonValue ) {
            NSLog(@"JSON value can't be retrieved due to error : %@", [error description] );
        }
        
        NSLog(@"JSON: %@", jsonValue);        
        
        if ( [[jsonValue objectForKey:@"success"] boolValue] ) {
            [[SharedHelper helper] showMessage:@"Task Available to work!" withColor:[UIColor greenColor]];
            self.acceptTaskConnection = nil;
        }
        
        else {
            [[SharedHelper helper] showMessage:@"This Task in not currently Available!" withColor:nil];            
            [self performSelector:@selector(popCurrentViewController) withObject:nil afterDelay:2.0];
        }
        [self.overlayView hide];
    }
    
    if ( connection == self.submitsTaskConnection ) {
        NSError *error = nil;
        NSDictionary *jsonValue = [NSJSONSerialization JSONObjectWithData:self.acceptReponseData options:NSJSONReadingAllowFragments error:&error];
        
        if ( !jsonValue ) {
            NSLog(@"JSON value can't be retrieved due to error : %@", [error description] );
        }
        
        NSLog(@"JSON: %@", jsonValue);
        
        if ( [[jsonValue objectForKey:@"success"] boolValue] ) {
            
            [[SharedHelper helper] showMessage:@"Task Submitted Successfully!" withColor:[UIColor greenColor]];
            [self performSelector:@selector(popCurrentViewController) withObject:nil afterDelay:2.0];
        }
        else
            [[SharedHelper helper] showMessage:@"Task can't be submitted at a moment! Please try again later!" withColor:nil];
        
        [self.overlayView hide];
        self.submitsTaskConnection = nil;
    }
}

@end
