//
//  TaskFormViewController.h
//  ArticleDigitization
//
//  Created by Rajan Maharjan on 9/14/12.
//  Copyright (c) 2012 Mystic Vision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tasks.h"
#import "TasksListViewController.h"

@interface TaskFormViewController : UIViewController <UITextViewDelegate, UIActionSheetDelegate, NSURLConnectionDelegate> {

    CGRect originalViewFrameBeforeKeyboardAppear;
    CGRect originalImageRect;
}

@property (nonatomic, strong) Tasks *currentTask;
@property (nonatomic, strong) TasksListViewController *tasksListViewController;

@end
