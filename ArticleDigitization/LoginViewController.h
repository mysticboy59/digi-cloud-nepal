//
//  LoginViewController.h
//  ArticleDigitization
//
//  Created by Rajan Maharjan on 9/14/12.
//  Copyright (c) 2012 Mystic Vision. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, NSURLConnectionDelegate> {
    
    CGRect originalViewFrameBeforeKeyboardAppear;
    
    NSURLConnection *loginConnection;
    NSMutableData *responseData;
    
    BOOL isCheckMarkChecked;
}

@end
