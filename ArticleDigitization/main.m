//
//  main.m
//  ArticleDigitization
//
//  Created by Rajan Maharjan on 9/13/12.
//  Copyright (c) 2012 Mystic Vision. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
