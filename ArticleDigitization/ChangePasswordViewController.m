//
//  ChangePasswordViewController.m
//  ArticleDigitization
//
//  Created by Rajan Maharjan on 9/15/12.
//  Copyright (c) 2012 Mystic Vision. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "SharedHelper.h"
#import "NSString+HTMLRemover.h"
#import "OverlayView.h"

@interface ChangePasswordViewController ()

@property (nonatomic, strong) IBOutlet UITextField *oldPassField;
@property (nonatomic, strong) IBOutlet UITextField *nwPassField;
@property (nonatomic, strong) IBOutlet UITextField *confirmPassField;

@property (nonatomic, strong) NSMutableData *changePasswordReponseData;
@property (nonatomic, strong) NSURLConnection *changePasswordConnection;
@property (nonatomic, strong) OverlayView *overlayView;

@end

@implementation ChangePasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma - self methods

- (OverlayView *) overlayView {
    if ( _overlayView != nil )
        return _overlayView;
    
    _overlayView = [[OverlayView alloc] initWithFrame:self.view.frame];
    return _overlayView;
}

- (NSMutableData *) changePasswordReponseData {
    if ( _changePasswordReponseData != nil )
        return _changePasswordReponseData;
    
    _changePasswordReponseData = [[NSMutableData alloc] init];
    return _changePasswordReponseData;
}

#pragma - Save Action 

- (BOOL) isDataValid {
    BOOL dataValid = YES;
        
    // Check for Null Conditions
    if ([self.nwPassField.text isEqualToString:@""] || [self.confirmPassField.text isEqualToString:@""] ) {
        dataValid = NO;
        [[SharedHelper helper] showMessage:@"Please fill all Fields!" withColor:nil];

        return dataValid;
    }
    
    // Check for matching New And Confirm Password    
    if ( ![self.nwPassField.text isEqualToString:self.confirmPassField.text]) {
        dataValid = NO;
        [[SharedHelper helper] showMessage:@"New Password couldn't be Confirmed!" withColor:[UIColor yellowColor]];
        return dataValid;
    }
    
    // Check for correct OLD Password    
    
    if ( ![self.oldPassField.text isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"UserPassword"]] ) {
        dataValid = NO;
        [[SharedHelper helper] showMessage:@"Old Password doesn't Match. Try again?" withColor:[UIColor yellowColor]];
        
    }
    
    return dataValid;
}

- (IBAction) saveAction :(id)sender {
    
    if ( ![self isDataValid] ) return;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *mobileNumber = [userDefaults objectForKey:@"UserMobileNumber"];
    NSString *token = [userDefaults objectForKey:@"UserToken"];
    NSString *oldPassword = [userDefaults objectForKey:@"UserPassword"];
    
    NSString *encodedMobileNumber = [mobileNumber urlEncodeUsingEncoding:kCFStringEncodingUTF8];
    NSString *encodedToken = [token urlEncodeUsingEncoding:kCFStringEncodingUTF8];
    NSString *encodedOldPassword = [oldPassword urlEncodeUsingEncoding:kCFStringEncodingUTF8];
    
    NSString *newPassword = self.confirmPassField.text;
    
    NSDictionary *JSONDictionary = [NSDictionary dictionaryWithObjects:
                                    [NSArray arrayWithObjects: encodedMobileNumber, encodedToken, encodedOldPassword, newPassword, nil]
                                                               forKeys:[NSArray arrayWithObjects: @"mobile_number", @"token", @"old_password", @"new_password", nil]];
    NSError *error = nil;
    
    // Converted JSONDictionary Object in JSON data Object
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:JSONDictionary
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *content = [NSString stringWithFormat:@"%@", jsonString];
    
    NSString *connectionString = [NSString stringWithFormat:@"%@/change_password", SERVER_STRING];
    
    NSURL* url = [NSURL URLWithString:connectionString];
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:url];
    
    [urlRequest setHTTPMethod:@"PUT"];
    [urlRequest setHTTPBody:[content dataUsingEncoding:NSUTF8StringEncoding]];
    
    self.changePasswordConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
    
    if (self.changePasswordConnection) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        [self.overlayView showInView:self.view withActivityIndicator:YES];
    }
    
}

#pragma - Keyboard Notifications

- (void) keyboardWillShow:(NSNotification *)notification {
    
    if ( isFirstTextFieldClicked ) return;
    
    CGFloat keyboardHeight;
    CGFloat animationDuration = 0.3;
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if(notification) {
        NSDictionary* keyboardInfo = [notification userInfo];
        CGRect keyboardFrame = [[keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
        animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
        
        if(UIInterfaceOrientationIsPortrait(orientation))
            keyboardHeight = keyboardFrame.size.height;
        else
            keyboardHeight = keyboardFrame.size.width;
    }
    
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    if(UIInterfaceOrientationIsPortrait(orientation))
        screenHeight = [UIScreen mainScreen].bounds.size.height;
    else
        screenHeight = [UIScreen mainScreen].bounds.size.width;
    
    __block CGRect frame = self.view.frame;
    
    originalViewFrameBeforeKeyboardAppear = frame;
    
    
    if (frame.origin.y + frame.size.height > screenHeight - keyboardHeight) {
        
        frame.origin.y = screenHeight - keyboardHeight - frame.size.height - 10;
        
        if (frame.origin.y < 0)
            frame.origin.y = 0;
        
        frame.origin.y = frame.origin.y - 100.0;
        
        
        [UIView animateWithDuration:animationDuration
                              delay:0.0
                            options:UIViewAnimationCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             self.view.frame = frame;
                         }
                         completion:nil];
    }
}

- (void) keyboardWillHide:(NSNotification *) notification {
    
    if ( isFirstTextFieldClicked ) return;
    
    CGFloat keyboardHeight;
    CGFloat animationDuration = 0.3;
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if(notification) {
        NSDictionary* keyboardInfo = [notification userInfo];
        CGRect keyboardFrame = [[keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
        animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
        
        if(UIInterfaceOrientationIsPortrait(orientation))
            keyboardHeight = keyboardFrame.size.height;
        else
            keyboardHeight = keyboardFrame.size.width;
    }
    
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    if(UIInterfaceOrientationIsPortrait(orientation))
        screenHeight = [UIScreen mainScreen].bounds.size.height;
    else
        screenHeight = [UIScreen mainScreen].bounds.size.width;
    
    
    __block CGRect frame = self.view.frame;
    
    if (frame.origin.y < screenHeight - keyboardHeight) {
        
        frame.origin.y = originalViewFrameBeforeKeyboardAppear.origin.y;
        
        [UIView animateWithDuration:animationDuration
                              delay:0.0
                            options:UIViewAnimationCurveEaseIn | UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             self.view.frame = frame;
                         }
                         completion:nil];
    }
}

#pragma  - UITextField Delegate

- (void) textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    
    if ( textField == self.oldPassField )
        isFirstTextFieldClicked = YES;
    
    return YES;
}

- (BOOL) textFieldShouldEndEditing:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if ( textField == self.oldPassField )
        isFirstTextFieldClicked = NO;
    
    [textField resignFirstResponder];
    
    if ( textField.returnKeyType == UIReturnKeyNext && textField == self.oldPassField)
        [self.nwPassField becomeFirstResponder];
    
    else if ( textField.returnKeyType == UIReturnKeyNext && textField == self.nwPassField)
        [self.confirmPassField becomeFirstResponder];
        
    else if ( textField.returnKeyType == UIReturnKeyGo )
        [self saveAction:nil];
    
    return YES;
}

- (BOOL) textFieldShouldClear:(UITextField *)textField {
    return YES;
}

#pragma - NSUrlConnection Delegates

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    if (connection == self.changePasswordConnection) {
        [self.changePasswordReponseData setLength:0];
    }    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if (connection == self.changePasswordConnection) {
        [self.changePasswordReponseData appendData:data];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if (connection == self.changePasswordConnection) {
        
        NSLog(@"Task Accpetance Failed : %@", [error description]);
        
        [self.overlayView hide];
        self.changePasswordConnection = nil;
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if (connection == self.changePasswordConnection) {
        
        // Check here validity of Login using JSON response
        
        NSError *error = nil;
        NSDictionary *jsonValue = [NSJSONSerialization JSONObjectWithData:self.changePasswordReponseData options:NSJSONReadingAllowFragments error:&error];
        
        if ( !jsonValue ) {
            NSLog(@"JSON value can't be retrieved due to error : %@", [error description] );
        }
        
        NSLog(@"JSON: %@", jsonValue);
        
        if ( [[jsonValue objectForKey:@"success"] boolValue] ) {
            [[SharedHelper helper] showMessage:@"Password Changed Successfully!" withColor:[UIColor greenColor]];
            self.changePasswordConnection = nil;
        }
        
        [self performSelector:@selector(popCurrentViewController) withObject:nil afterDelay:2.0];
    }
}

- (void) popCurrentViewController {

    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
