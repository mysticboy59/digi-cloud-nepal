//
//  TasksListViewController.m
//  ArticleDigitization
//
//  Created by Rajan Maharjan on 9/14/12.
//  Copyright (c) 2012 Mystic Vision. All rights reserved.
//

#import "TasksListViewController.h"
#import "SharedHelper.h"
#import "TaskFormViewController.h"
#import "NSString+HTMLRemover.h"

#import "Tasks.h"

@interface TasksListViewController ()

@end

@implementation TasksListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    self.view.backgroundColor = [UIColor scrollViewTexturedBackgroundColor];
    
    taskListResponseData = [[NSMutableData alloc] init];
    
    [self retriveTaskListFromServer];
}

- (void) viewWillAppear:(BOOL)animated {
    [self retriveTaskListFromServer];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma - Custom Methods

- (OverlayView *) overlayView {
    
    if( _overlayView != nil )
        return _overlayView;
    
    _overlayView = [[OverlayView alloc] initWithFrame:self.view.frame];
    return _overlayView;    
}

- (void) retriveTaskListFromServer {

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *mobileNumber = [userDefaults objectForKey:@"UserMobileNumber"];
    NSString *token = [userDefaults objectForKey:@"UserToken"];
    
	NSString *encodedMobileNumber = [mobileNumber urlEncodeUsingEncoding:kCFStringEncodingUTF8];
	NSString *encodedToken = [token urlEncodeUsingEncoding:kCFStringEncodingUTF8];
    
    NSDictionary *JSONDictionary = [NSDictionary dictionaryWithObjects:
                                    [NSArray arrayWithObjects: encodedMobileNumber, encodedToken , nil] forKeys:[NSArray arrayWithObjects: @"mobile_number", @"token", nil]];    
    NSError *error = nil;
    
    // Converted JSONDictionary Object in JSON data Object
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:JSONDictionary
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *content = [NSString stringWithFormat:@"%@", jsonString];
    
	NSString *connectionString = [NSString stringWithFormat:@"%@/get_tasks", SERVER_STRING];
    
	NSURL* url = [NSURL URLWithString:connectionString];
	NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:url];
    
	[urlRequest setHTTPMethod:@"POST"];
	[urlRequest setHTTPBody:[content dataUsingEncoding:NSUTF8StringEncoding]];
        
	taskListsConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
    
	if (taskListsConnection) {
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        [self.overlayView showInView:self.view withActivityIndicator:YES];
	}
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [taskListsObjects count];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if ( cell == nil )
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    
    //Configure Cell here

    Tasks *taskObject = [taskListsObjects objectAtIndex:indexPath.row];
    
    cell.textLabel.text = taskObject.title;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Chunk : %d  Reward: Rs %d", taskObject.chunkNO, taskObject.rewardAmount];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.

    TaskFormViewController *detailViewController = [[TaskFormViewController alloc] initWithNibName:@"TaskFormViewController" bundle:nil];

    detailViewController.currentTask = [taskListsObjects objectAtIndex:indexPath.row];

    if ( detailViewController.tasksListViewController == nil )
        detailViewController.tasksListViewController = self;
    // ...
    // Pass the selected object to the new view controller.
    detailViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];

}

#pragma - NSUrlConnection Delegates

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    if (connection == taskListsConnection) {
        [taskListResponseData setLength:0];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if (connection == taskListsConnection) {
        [taskListResponseData appendData:data];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if (connection == taskListsConnection) {
        
        NSLog(@"Task List Retrival Failed : %@", [error description]);
        
        [self.overlayView hide];
        taskListsConnection = nil;
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if (connection == taskListsConnection) {
        
        // Check here validity of Login using JSON response

        NSError *error = nil;        
        NSDictionary *jsonValue = [NSJSONSerialization JSONObjectWithData:taskListResponseData options:NSJSONReadingAllowFragments error:&error];
        
        if ( !jsonValue ) {
            NSLog(@"JSON value can't be retrieved due to error : %@", [error description] );
        }
        
        NSLog(@"JSON: %@", jsonValue);
        
        
        if ( [[jsonValue objectForKey:@"success"] boolValue] ) {
            
            NSArray * dictionaryArray = [[NSMutableArray alloc] initWithArray:[jsonValue objectForKey:@"tasks"]];
            taskListsObjects = [[NSMutableArray alloc] init];
            
            // Convert dictionary items into Objects
            
            for (int i = 0; i < [dictionaryArray count]; i++) {
                
                Tasks *taskObject = [[Tasks alloc] init];
                
                taskObject.articleID = [[[dictionaryArray objectAtIndex:i] objectForKey:@"article_id"] integerValue];
                taskObject.chunkImagePath = [[dictionaryArray objectAtIndex:i] objectForKey:@"chunk_image"];
                taskObject.chunkNO = [[[dictionaryArray objectAtIndex:i] objectForKey:@"chunk_no"] integerValue];
                taskObject.postedDate = [[dictionaryArray objectAtIndex:i] objectForKey:@"posted_date"];
                taskObject.rewardAmount = [[[dictionaryArray objectAtIndex:i] objectForKey:@"reward_amount"] integerValue];
                taskObject.title = [[dictionaryArray objectAtIndex:i] objectForKey:@"title"];
                
                [taskListsObjects addObject:taskObject];
            }
        }
        
        if ( [taskListsObjects count] == 0 )
            [[SharedHelper helper] showMessage:@"No Tasks are currently Available" withColor:[UIColor yellowColor]];
        
        
        [self.tableView reloadData];
        [self.overlayView hide];
        taskListsConnection = nil;
    }
}


@end