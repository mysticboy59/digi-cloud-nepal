//
//  TransactionHistoryViewController.h
//  ArticleDigitization
//
//  Created by Rajan Maharjan on 9/15/12.
//  Copyright (c) 2012 Mystic Vision. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionHistoryViewController : UITableViewController

@property (nonatomic, strong) NSArray *transactionHistoryArray;

@end
