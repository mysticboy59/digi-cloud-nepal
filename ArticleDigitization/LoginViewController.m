//
//  LoginViewController.m
//  ArticleDigitization
//
//  Created by Rajan Maharjan on 9/14/12.
//  Copyright (c) 2012 Mystic Vision. All rights reserved.
//

#import "LoginViewController.h"
#import "SharedHelper.h"
#import "NSString+HTMLRemover.h"

#import "TaskFormViewController.h"

@interface LoginViewController ()

@property (nonatomic, strong) IBOutlet UITableView *loginTableView;
@property (nonatomic, strong) IBOutlet UITextField *mobileNumberTextField;
@property (nonatomic, strong) IBOutlet UITextField *passwordTextField;
@property (nonatomic, strong) IBOutlet UIButton *loginInButton;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) IBOutlet UIButton *checkMarkButton;

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        responseData = [[NSMutableData alloc] init];        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.loginTableView.backgroundColor = [UIColor clearColor];
    self.title = @"Article Digitization";
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    self.activityIndicator.hidden = YES;
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( [userDefaults objectForKey:@"UserMobileNumber"] ) {
        // Load saved User Details in TextField;
        self.mobileNumberTextField.text = [userDefaults objectForKey:@"UserMobileNumber"];
    }
    
    if ( [userDefaults boolForKey:@"AutoFillPassword"] ) {
        self.passwordTextField.text = [userDefaults objectForKey:@"UserPassword"];
        isCheckMarkChecked = YES;
    }
    
    if ( isCheckMarkChecked )
        [self.checkMarkButton setImage:[UIImage imageNamed:@"Checkbox-On"] forState:UIControlStateNormal];

    else
        [self.checkMarkButton setImage:[UIImage imageNamed:@"Checkbox-Off"] forState:UIControlStateNormal];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma - Methods 

- (BOOL) isDataValid {
    BOOL dataValid = YES;
    if ([self.mobileNumberTextField.text isEqualToString:@""] || [self.passwordTextField.text isEqualToString:@""]) {
        dataValid = NO;
        [[SharedHelper helper] showMessage:@"Please fill all Fields!" withColor:nil];
        [self enableLoginUIElements];
    }
    return dataValid;
}

- (IBAction) login:(id)sender {
    [self disableLoginUIElements];
    [ self loginToServer ];
}

- (IBAction) checkMarkButtonClicked:(id)sender {
    
    if ( !isCheckMarkChecked ) {
        
        [self.checkMarkButton setImage:[UIImage imageNamed:@"Checkbox-On"] forState:UIControlStateNormal];
        isCheckMarkChecked = YES;
    }
    
    else {
        [self.checkMarkButton setImage:[UIImage imageNamed:@"Checkbox-Off"] forState:UIControlStateNormal];
        isCheckMarkChecked = NO;
    }
    
}

-(void) loginToServer {
    
    if ( ![self isDataValid] ) return;
    
    NSString *mobileNumber = self.mobileNumberTextField.text;
    NSString *password = self.passwordTextField.text;

	NSString *encodedUsername = [mobileNumber urlEncodeUsingEncoding:kCFStringEncodingUTF8];
	NSString *encodedPassword = [password urlEncodeUsingEncoding:kCFStringEncodingUTF8];
    
    NSDictionary *JSONDictionary = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects: encodedUsername, encodedPassword,nil] forKeys:[NSArray arrayWithObjects: @"mobile_number", @"password", nil]];

    NSError *error = nil;
    
    // Converted JSONDictionary Object in JSON data Object
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:JSONDictionary
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *content = [NSString stringWithFormat:@"%@", jsonString];

	NSString *connectionString = [NSString stringWithFormat:@"%@/login", SERVER_STRING];
    
	NSURL* url = [NSURL URLWithString:connectionString];
	NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:url];
    
	[urlRequest setHTTPMethod:@"POST"];
	[urlRequest setHTTPBody:[content dataUsingEncoding:NSUTF8StringEncoding]];
    
	loginConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];

	if (loginConnection) {
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	}
}

#pragma - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"LoginCell";

    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if ( cell == nil )
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    cell.textLabel.text = @"";
    
    return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
	return nil;
}

#pragma mark - UITableViewDelegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

#pragma - Keyboard Notifications

- (void) keyboardWillShow:(NSNotification *)notification {
    
    CGFloat keyboardHeight;
    CGFloat animationDuration = 0.3;
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if(notification) {
        NSDictionary* keyboardInfo = [notification userInfo];
        CGRect keyboardFrame = [[keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
        animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
        
        if(UIInterfaceOrientationIsPortrait(orientation))
            keyboardHeight = keyboardFrame.size.height;
        else
            keyboardHeight = keyboardFrame.size.width;
    }
    
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    if(UIInterfaceOrientationIsPortrait(orientation))
        screenHeight = [UIScreen mainScreen].bounds.size.height;
    else
        screenHeight = [UIScreen mainScreen].bounds.size.width;
    
    __block CGRect frame = self.view.frame;
    
    originalViewFrameBeforeKeyboardAppear = frame;
    
    
    if (frame.origin.y + frame.size.height > screenHeight - keyboardHeight) {
        
        frame.origin.y = screenHeight - keyboardHeight - frame.size.height - 10;
        
        if (frame.origin.y < 0)
            frame.origin.y = 0;
        
        frame.origin.y = frame.origin.y - 100.0;
        
        
        [UIView animateWithDuration:animationDuration
                              delay:0.0
                            options:UIViewAnimationCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             self.view.frame = frame;
                         }
                         completion:nil];
    }
}

- (void) keyboardWillHide:(NSNotification *) notification {
    
    CGFloat keyboardHeight;
    CGFloat animationDuration = 0.3;
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if(notification) {
        NSDictionary* keyboardInfo = [notification userInfo];
        CGRect keyboardFrame = [[keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
        animationDuration = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
        
        if(UIInterfaceOrientationIsPortrait(orientation))
            keyboardHeight = keyboardFrame.size.height;
        else
            keyboardHeight = keyboardFrame.size.width;
    }
    
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    if(UIInterfaceOrientationIsPortrait(orientation))
        screenHeight = [UIScreen mainScreen].bounds.size.height;
    else
        screenHeight = [UIScreen mainScreen].bounds.size.width;
    
    
    __block CGRect frame = self.view.frame;
    
    if (frame.origin.y < screenHeight - keyboardHeight) {
        
        frame.origin.y = originalViewFrameBeforeKeyboardAppear.origin.y;
        
        [UIView animateWithDuration:animationDuration
                              delay:0.0
                            options:UIViewAnimationCurveEaseIn | UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             self.view.frame = frame;
                         }
                         completion:nil];
    }
}

#pragma  - UITextField Delegate

- (void) textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
    
}

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    
    textField.keyboardType = UIKeyboardTypeNamePhonePad;
    return YES;
}

- (BOOL) textFieldShouldEndEditing:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if ( textField.returnKeyType == UIReturnKeyNext )
        [self.passwordTextField becomeFirstResponder];
    
    else if ( textField.returnKeyType == UIReturnKeyGo )
        [self login:nil];
    
    return YES;
}

- (BOOL) textFieldShouldClear:(UITextField *)textField {
    return YES;
}

#pragma - NSUrlConnection Delegates

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    if (connection == loginConnection)
        [responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if (connection == loginConnection)
        [responseData appendData:data];    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    [self enableLoginUIElements];
    
    if ( connection == loginConnection ) {
        NSLog(@"Login Failed : %@", [error description]);
        loginConnection = nil;        
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {

    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

    [self enableLoginUIElements];
    
    if (connection == loginConnection) {
        
        // Check here validity of Login using JSON response
        NSError *error = nil;
        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:&error];

        if ( !jsonResponse ) {
            NSLog(@"Error is: %@", [error description]);
        }        
        
        BOOL loginSuccess = [[jsonResponse objectForKey:@"success"] boolValue] ? YES : NO;
        
        if ( loginSuccess ) {
            
            // Save Token for Future Calls
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            
            [userDefaults setObject:[jsonResponse objectForKey:@"token"] forKey:@"UserToken"];
            [userDefaults setObject:self.mobileNumberTextField.text forKey:@"UserMobileNumber"];
            [userDefaults setObject:self.passwordTextField.text forKey:@"UserPassword"];
            
            if ( isCheckMarkChecked )
                [userDefaults setBool:YES forKey:@"AutoFillPassword"];

            else
                [userDefaults removeObjectForKey:@"AutoFillPassword"];
            
            [userDefaults synchronize];
            [APP_DELEGATE showTabBarControllerAfterLogin:self.view];
            [[SharedHelper helper] showMessage:@"Login successful!" withColor:[UIColor greenColor]];
        }
        
        else {
            [[SharedHelper helper] showMessage:@"Invalid Login Credential!" withColor:nil];
        }
        
        loginConnection = nil;
    }    
}

#pragma - Custom Methods

- (void) enableLoginUIElements {

    self.activityIndicator.hidden = YES;
    [self.activityIndicator stopAnimating];
    
    self.mobileNumberTextField.enabled = YES;
    self.passwordTextField.enabled = YES;
    self.loginInButton.enabled = YES;
    self.checkMarkButton.enabled = YES;
}

- (void) disableLoginUIElements {

    self.activityIndicator.hidden = NO;
    [self.activityIndicator startAnimating];
    
    self.mobileNumberTextField.enabled = NO;
    self.passwordTextField.enabled = NO;
    self.loginInButton.enabled = NO;
    self.checkMarkButton.enabled = NO;
}

@end
