//
//  TasksListViewController.h
//  ArticleDigitization
//
//  Created by Rajan Maharjan on 9/14/12.
//  Copyright (c) 2012 Mystic Vision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OverlayView.h"

@interface TasksListViewController : UITableViewController <NSURLConnectionDelegate> {
 
    NSMutableArray *taskListsObjects;
    
    NSURLConnection *taskListsConnection;
    NSMutableData * taskListResponseData;
}


@property (nonatomic, strong) OverlayView *overlayView;

- (void) retriveTaskListFromServer;

@end
