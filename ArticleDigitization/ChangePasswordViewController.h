//
//  ChangePasswordViewController.h
//  ArticleDigitization
//
//  Created by Rajan Maharjan on 9/15/12.
//  Copyright (c) 2012 Mystic Vision. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordViewController : UIViewController <UITextFieldDelegate, NSURLConnectionDelegate> {
    CGRect originalViewFrameBeforeKeyboardAppear;
    BOOL isFirstTextFieldClicked;    
}

@end
