//
//  AppDelegate.h
//  ArticleDigitization
//
//  Created by Rajan Maharjan on 9/13/12.
//  Copyright (c) 2012 Mystic Vision. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *navigationController;

@property (strong, nonatomic) UINavigationController *dashBoardNavController;
@property (strong, nonatomic) UITabBarController *tabBarController;

+ (BOOL) hasConnectivity;
- (void) showTabBarControllerAfterLogin:(UIView *) currentView;
- (void) logOutFromSystem;

@end
