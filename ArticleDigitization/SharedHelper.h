//
//  SharedHelper.h
//  iTuga
//
//  Created by Rajan Maharjan on 5/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

#define APP_DELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

#define SERVER_STRING   @"http://localhost/ad"

#define IPHONE_WIDTH    320
#define IPHONE_HEIGHT   480

#define IPAD_WIDTH      768
#define IPAD_HEIGHT     1004

@interface SharedHelper : NSObject

@property (nonatomic, strong) UIColor *navigationBarColor;
@property (nonatomic, strong) UIColor *backColorForViews;

+ (SharedHelper *) helper;

- (NSString *) fullNameWithFirstName:(NSString *) firstName middleName:(NSString *) middleName lastName:(NSString *) lastName;

- (UIView *) tableviewBackgroundViewInView:(UIView *) view;

- (UIView *) clearBackgroundColorOfTableViewInView:(UIView *) view;

- (void) showMessage:(NSString *) message withColor:(UIColor *) messageColor;

/* Saving Image to Libary */
- (void) saveImageToPhotoLibrary:(UIImage *) passedImage;

- (UIImage *) takeScreenShot;

@end
