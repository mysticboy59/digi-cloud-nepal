//
//  SharedHelper.m
//  iTuga
//
//  Created by Rajan Maharjan on 5/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SharedHelper.h"
#import "Toast.h"
#import <QuartzCore/QuartzCore.h>

@interface SharedHelper ()

@property (nonatomic, strong) UIView *tableViewBackgroundView;
@property (nonatomic, strong) UIColor *toastColor;

@end

@implementation SharedHelper

static SharedHelper * _helper = nil;

+ (SharedHelper *) helper {
    
    @synchronized ([SharedHelper class]) {
    
        if (!_helper) 
            _helper = [[self alloc] init];
        return _helper;
    }
    return nil;    
}

- (UIView *) tableViewBackgroundView {
    
    if (_tableViewBackgroundView != nil) 
        return _tableViewBackgroundView;

    _tableViewBackgroundView = [[UIView alloc] init];
    
    return _tableViewBackgroundView;    
}

- (id) init {
    
    if ( ( self = [super init] ) ) {
        
        self.navigationBarColor = [UIColor colorWithRed:72 / 255.0 green:110 / 255.0 blue:1 alpha:1.0];
        self.backColorForViews = [UIColor colorWithRed:100.0/255 green:149.0/255 blue:237.0/255 alpha:1.0];
    }
    return self;
}

- (NSString *) fullNameWithFirstName:(NSString *) firstName middleName:(NSString *) middleName lastName:(NSString *) lastName {
    
    NSMutableString *name = [[NSMutableString alloc] initWithString:firstName];

	if (middleName)
		[name appendFormat:@" %@", middleName];

	if (lastName)
		[name appendFormat:@" %@", lastName];
	
	return name;
}

- (UIView *) tableviewBackgroundViewInView:(UIView *) view {
    self.tableViewBackgroundView.frame = view.frame;
    self.tableViewBackgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"pattern"]];    
    return self.tableViewBackgroundView;
}

- (UIView *) clearBackgroundColorOfTableViewInView:(UIView *) view {
    self.tableViewBackgroundView.frame = view.frame;
    self.tableViewBackgroundView.backgroundColor = [UIColor clearColor];
    return self.tableViewBackgroundView;
}

// Send nil for default red color message

- (void) showMessage:(NSString *) message withColor:(UIColor *) messageColor {
    Toast *toast = [[Toast alloc] initWithMessage:message];
    
    toast.tint = messageColor ? messageColor : self.toastColor;
    toast.toastVisibilityDuration = TOAST_VISIBILITY_DURATION_ONE_SEC;				
    [toast showInView: [APP_DELEGATE window]];
}

- (UIColor *) toastColor {
    if (_toastColor != nil)
        return _toastColor;
    
    _toastColor = [UIColor redColor];
    
    return _toastColor;
}

- (UIImage * ) mergeImage:(UIImage *)firstImage toImage:(UIImage *)secondImage strength:(float)alpha {
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(firstImage.size.width, firstImage.size.height), YES, 0.0); 
    
    [firstImage drawAtPoint: CGPointMake(0,0)];
    
    [secondImage drawAtPoint: CGPointMake(0,0) blendMode: kCGBlendModeNormal alpha: alpha]; // 0 - 1
    
    UIImage *mergedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return mergedImage; 
}

- (void) saveImageToPhotoLibrary:(UIImage *) passedImage {

    UIImageWriteToSavedPhotosAlbum(passedImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);    
}


#pragma - ImageSave Delegate
- (void) image: (UIImage *) image didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo {
    if ( error )
        NSLog(@"Error : %@", [error description]);
}

- (UIImage *) takeScreenShot {
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
        UIGraphicsBeginImageContextWithOptions(APP_DELEGATE.window.bounds.size, NO, [UIScreen mainScreen].scale);
    else
        UIGraphicsBeginImageContext(APP_DELEGATE.window.bounds.size);
    
    [[APP_DELEGATE window].layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
