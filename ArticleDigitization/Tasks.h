//
//  Tasks.h
//  ArticleDigitization
//
//  Created by Rajan Maharjan on 9/16/12.
//  Copyright (c) 2012 Mystic Vision. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tasks : NSObject


@property (nonatomic, assign) NSUInteger articleID;
@property (nonatomic, assign) NSUInteger chunkNO;
@property (nonatomic, assign) NSUInteger rewardAmount;
@property (nonatomic, strong) NSString *chunkImagePath;
@property (nonatomic, strong) NSString *postedDate;
@property (nonatomic, strong) NSString *title;

@end
