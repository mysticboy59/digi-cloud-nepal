//
//  SettingsViewController.h
//  ArticleDigitization
//
//  Created by Rajan Maharjan on 9/14/12.
//  Copyright (c) 2012 Mystic Vision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OverlayView.h"

@interface SettingsViewController : UITableViewController  <NSURLConnectionDelegate>{

    NSArray *sectionHeaders;
    NSArray *profileRowsLabels;
    NSArray *earningRowsLabels;
    
    NSArray *userProfileObjectsArray;
    NSMutableArray *userEarningObjectsArray;
    
    // NSURL Connection
    
    NSURLConnection *logoutConnection;
    NSMutableData *logoutResponseData;    
}

@property (nonatomic, strong) OverlayView *overlayView;

@end
