//
//  NSString+HTMLRemover.h
//  TotalAutoLink
//
//  Created by Rajan Maharjan on 5/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (extension)

-(NSString *) stringByStrippingHTML;

@end

@interface NSString (URLEncoding)

-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding;

@end
