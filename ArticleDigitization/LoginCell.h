//
//  LoginCell.h
//  ArticleDigitization
//
//  Created by Rajan Maharjan on 9/14/12.
//  Copyright (c) 2012 Mystic Vision. All rights reserved.
//

#import <UIKit/UIKit.h>

//@protocol LoginCellDelegate <NSObject>
//
//- (void) textFieldDidEndEditing:(UITextField *) textField andIndexPath:(NSIndexPath *) indexPath;
//- (BOOL) textFieldShouldBeginEditing:(UITextField *) textField andIndexPath:(NSIndexPath *) indexPath;
//- (BOOL) textFieldShouldReturn:(UITextField *)textField andIndexPath:(NSIndexPath *) indexPath;
//
//@end


@interface LoginCell : UITableViewCell <UITextFieldDelegate>

@property (nonatomic, retain) NSIndexPath *indexPath;
@property (nonatomic, strong) IBOutlet UITextField *cellTextField;

@end
